import matplotlib
from Model.CryptoCurrency import CryptoCurrency
import matplotlib.pyplot as plt


class Display:
    def CreateDisplay(self, cryptos):

        names = []
        totalvalue = []

        for coin in cryptos:
            names.append(coin.Name)
            totalvalue.append(coin.Value)

        fig = plt.figure()
        ax = fig.add_subplot(111)
        # names = ['C', 'C++', 'Java', 'Python', 'PHP']
        # students = [23, 17, 35, 29, 12]

        for x in cryptos:
            plt.plot(x.HistoricDates, x.HistoricValue, label=x.Name)
            plt.text(x.HistoricDates[-1], x.HistoricValue[-1], x.Name)

        plt.xlabel('x')
        plt.ylabel('y')

        # ax.bar(names, totalvalue)
        # plt.legend()
        plt.show(block=True)
