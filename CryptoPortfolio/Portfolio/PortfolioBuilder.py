import json

from API.CoinGeckoAPI import CoinGeckoAPI
from Display.Display import Display
from Helpers.csvUtil import csvUtil
from datetime import datetime


class PortfolioBuilder:

    def __init__(self):
        with open("config.json") as json_data_file:
            self.Data = json.load(json_data_file)

    def Run(self):
        csv = csvUtil()
        coins = csv.read(self.Data['csvDir'])

        api = CoinGeckoAPI()

        coinNames = []

        for x in coins:
            coinNames.append(x.Name)

        prices = api.get_price(ids=coinNames, vs_currencies=['USD'], include_market_cap='true')

        for i in coins:
            historic = api.get_coin_market_chart_by_id(i.Name.lower(), 'USD', 7)
            i.HistoricValue = []
            i.HistoricDates = []
            for date in historic['prices']:
                i.HistoricValue.append(float(date[1]) * float(i.Quantity))
                i.HistoricDates.append(datetime.fromtimestamp(date[0]/1000))



        totalVal = 0

        for i in coins:
            value = float(i.Quantity) * prices[i.Name.lower()]['usd']
            # print(i.Name + ': ' + str(value))
            i.Value = value
            totalVal += value

        print('########################')
        print('Total: $' + str(totalVal))
        print('########################')

        display = Display()
        display.CreateDisplay(coins)


