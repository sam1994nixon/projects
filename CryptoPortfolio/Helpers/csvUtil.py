import csv

from Model.CryptoCurrency import CryptoCurrency


class csvUtil:

    def read(self, dir):
        cryptos = []
        with open(dir) as dataFile:
            data = csv.DictReader(dataFile)
            for row in data:
                cryptos.append(CryptoCurrency(row['Name'], row['Quantity']))

        return cryptos
